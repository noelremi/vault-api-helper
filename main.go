package vault

import (
	"io"
	"net/http"
	"net/url"

	"github.com/pkg/errors"
)

// API represents the basic configuration to access vault
type API struct {
	VaultAddr  string
	VaultToken string
}

// NewRequest returns a configured http.Request for the Vault API
func (api *API) NewRequest(method string, path string, body io.Reader) (*http.Request, error) {
	// Parse the URL provided
	url, err := url.Parse(api.VaultAddr)
	if err != nil {
		return nil, errors.Wrap(err, "Vault address parsing failed")
	}

	// Add the request path to the URL
	url.Path = path

	// Create a new request
	req, err := http.NewRequest(method, url.String(), body)
	if err != nil {
		return nil, errors.Wrap(err, "Creation of the http.Request failed")
	}

	// Standard Vault API request header
	req.Header.Add("X-Vault-Request", `true`)

	// Check if the Vault token is set
	if api.VaultToken == "" {
		return nil, errors.New("The Vault token is not set. Set a token or use the Login() method")
	}
	// If it was set, add it the the request header
	req.Header.Add("X-Vault-Token", api.VaultToken)

	return req, err
}
